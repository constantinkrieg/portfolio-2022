import React from 'react';
import ContactForm from '../ContactFrom'

const ContactSection = () => {

    return(
        <section id="contact" className="tp-contact-pg-section section-padding">
            <div className="container">
                <div className="row">
                    <div className="col col-lg-10 offset-lg-1">
                        <div className="office-info">
                            <div className="row">
                                <div className="col col-xl-6 col-lg-6 col-md-6 col-12">
                                    <a href="mailto:constantinkrieg@gmail.com">
                                        <div className="office-info-item">
                                            <div className="office-info-icon">
                                                <div className="info-icon">
                                                    <i className="fi flaticon-mail"></i>
                                                </div>
                                            </div>
                                            <div className="office-info-text">
                                                <h2>E-mail</h2>
                                                <p>constantinkrieg@gmail.com</p>
                                            </div>
                                        </div>
                                    </a>
                                </div> 
                                <div className="col col-xl-6 col-lg-6 col-md-6 col-12">
                                    <a href="tel:+33543460886">
                                    <div className="office-info-item">
                                        <div className="office-info-icon">
                                            <div className="info-icon">
                                                 <i className="fi flaticon-telephone"></i>
                                            </div>
                                        </div>
                                        <div className="office-info-text">
                                            <h2>Téléphone</h2>
                                            <p>06 43 46 08 86</p>
                                        </div>
                                    </div>
                                    </a>
                                </div> 
                            </div>
                        </div>
                        <div className="section-title section-title2 text-center">
                            <span>Contact</span>
                            <h2 class="no-uppercase">N'hésitez pas à me contacter !</h2>
                        </div>
                        <div className="tp-contact-form-area">
                            <ContactForm/>
                        </div>
                    </div>                
                </div>
            </div> 
            {/*
            <section className="tp-contact-map-section">
                <div className="tp-contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.9147703055!2d-74.11976314309273!3d40.69740344223377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbd!4v1547528325671"></iframe>
                </div>
            </section>
            */}
        </section>
     )
        
}

export default ContactSection;
