import React from 'react'
import DefaultModal from '../AboutModal'
import Image from 'next/image'


const About = (props) => {

    return(
        <section id="about" className="tp-about-section">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-5 col-md-12 col-12">
                        <div className="tp-about-wrap">
                        <div className="tp-about-img">
                            <div className="about-picture"></div>
                            <Image src="/static/images/about.webp" alt="Constantin Krieg"  width="430" height="550"/>
                        </div>
                        </div>
                    </div>
                    <div className="col-lg-7 col-md-12 col-12">
                        <div className="tp-about-text">
                        <div className="tp-about-icon">
                            <i className="fi flaticon-user"></i>
                        </div>
                        <div className="tp-about-icon-content">
                            <h2>Je suis...</h2>
                            <p className="light">
                                Un passioné des interfaces, des interactions entre l'utilisateur et son environnement ainsi que des nouvelles technologies.
                                Diplômé d'un master en Design Interactif à E-artsup Paris, j'ai acquis et maitrisé de nombreuses compétences autour de l'UI et l'UX Design tout en ayant une rigueur sur la réalisation de tous les médias de communication (affiche print, vidéo ou animations, web, social ...). 
                            </p>
                            <p className="light">
                                De part ma curiosité et mon autonomie, j'ai appris à coder et développer afin de donner vie aux projets en plus d'en concevoir le design. Ces deux compétence une fois associé, me permet d'avoir une meilleure communication entre les designers et les développeurs afin de pousser des projets avec une rigueur et une rapidité dans l'éxecution.
                            </p>
                            <a href="/pdf/CV.pdf" className="hero-btn">Téléchargez mon CV</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="black_svg svg_black">
                <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
                    <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
                </svg>
            </div>
        </section>
    )
}

export default About;