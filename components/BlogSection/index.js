import React, { useState } from 'react';
import BlogSingle from '../BlogSingle'
import Link from 'next/link'
import Image from 'next/image'



const blog = [
    {
        Id:"1",
        heading:"#UX - L'importance du 'storytelling'",
        bImg1:'/static/images/blog/blog1/1.jpg',
        author:'/static/images/blog/blog1/author.jpg',
        authorName:"Constantin",
        date:"4 Fév 2022",
        content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        quote:'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
        tag:'UX/UI Design'
    },
    {
        Id:"2",
        heading:"#branding - Oui SNCF devient SNCF Connect",
        bImg1:'/static/images/blog/blog1/2.jpg',
        author:'/static/images/blog/blog1/author2.jpg',
        authorName:"Constantin",
        date:"28 Jan 2022",
        content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        quote:'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
        tag:'Design',
    },
    {
        Id:"3",
        heading:"#CES2022 - Mes coups de coeur du salon",
        bImg1:'/static/images/blog/blog1/3.jpg',
        author:'/static/images/blog/blog1/author3.jpg',
        authorName:"Constantin",
        date:"17 Jan 2022",
        content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        quote:'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
        tag:'Expositions'
    },
]



const BlogSection = () => {

    const [open, setOpen] = React.useState(false);

    function handleClose() {
        setOpen(false);
    }

    const [state,setState] = useState({
        
    })

    const handleClickOpen = (item) =>{
        setOpen(true);
        setState(item)
    }

    return (
        <section id="blog" className="blog-section section-padding">
            <div className="container d-none">
                <div className="col-l2">
                    <div className="section-title section-title2 text-center">
                        <span>Blog & Veille</span>
                        <h2>Articles récents</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-xs-12">
                        <div className="blog-grids clearfix">
                            {blog.map((blogs, bl) => (
                                <div className="grid" key={bl}>
                                    <div className="entry-media">
                                        <Image src={blogs.bImg1} alt="Image de l'article" width="360" height="235" />
                                    </div>
                                    <div className="details">
                                    <h3 onClick={()=> handleClickOpen(blogs)}>{blogs.heading}</h3>
                                        <ul className="entry-meta d-flex">
                                            <li class="d-flex align-items-center">
                                            <Image src={blogs.author} alt="Auteur de l'article" width="40" height="40" />
                                                <span className="ml-2">par </span>
                                                <button  onClick={()=> handleClickOpen(blogs)}>{blogs.authorName}</button>
                                            </li>
                                            <li>{blogs.date}</li>
                                        </ul>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div> 
            <div className="white_svg">
                <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
                    <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
                </svg>
            </div>
            <BlogSingle open={open} onClose={handleClose} title={state.heading} content={state.content} quote={state.quote} image1={state.bImg1} author={state.author} authorName={state.authorName} date={state.date} tag={state.tag}/>
        </section>
    );
}
export default BlogSection;