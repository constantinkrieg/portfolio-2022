
import React, { Fragment } from 'react';
import Link from 'next/link'
import {Dialog, Grid, } from '@material-ui/core'
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Image from 'next/image'


const BlogSingle = ({ maxWidth, open, onClose, title, content, quote, image1,author,authorName,tag,date}) => {

    const styles = (theme) => ({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

    const DialogTitle = withStyles(styles)((props) => {
        const { children, classes, onClose, ...other } = props;
        return (
            <MuiDialogTitle disableTypography className={classes.root} {...other}>
                <Typography variant="h6">{children}</Typography>
                {onClose ? (
                    <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                        <i className="fa fa-close"></i>
                    </IconButton>
                ) : null}
            </MuiDialogTitle>
        );
    });


    return (
        <Fragment>
            <Dialog
                open={open}
                onClose={onClose}
                className="modalWrapper quickview-dialog"
                maxWidth={maxWidth}
            >
                <DialogTitle id="customized-dialog-title" onClose={onClose}>

                </DialogTitle>
                <Grid className="modalBody modal-body tp-blog-single-section">
                    <div className="tp-blog-content clearfix">
                        <div className="post">
                            <div className="entry-media">
                                <Image src={image1} alt="Image de l'article" width="888" height="580"/>
                            </div>
                            <ul className="entry-meta">
                                <li>
                                    <Image src={author} alt="Auteur de l'article" width="40" height="40"/>
                                    <Link href="/"><a>par {authorName}</a></Link>
                                </li>
                                <li><Link href="/"><a><i className="fa fa-calendar-o" aria-hidden="true"></i>{date}</a></Link></li>
                            </ul>
                            <h2>{title}</h2>
                            <p>{content}</p>
                            <blockquote>{quote}</blockquote>
                        </div>

                        <div className="tag-share clearfix">
                            <div className="tag">
                                <ul>
                                    <li><Link href="/"><a>{tag}</a></Link></li>
                                </ul>
                            </div>
                            <div className="share">
                                <ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </Grid>
            </Dialog>
        </Fragment>
    );
}
export default BlogSingle

