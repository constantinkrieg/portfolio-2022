import React, { useState } from 'react';
import ServiceSingle from '../ServiceSingle'
import { Button } from '@material-ui/core'


const Service = () => {

    const [open, setOpen] = React.useState(false);

    function handleClose() {
        setOpen(false);
    }

    const [state,setState] = useState({
    })

    const handleClickOpen = (item) =>{
        setOpen(true);
        setState(item)
    }
    const service = [
        {
            Id:"1",
            sIcon:"fi flaticon-brain",
            heading:"UX Design",
            Simg1:'images/service-single/web-design/img-1.jpg',
            Simg2:'images/service-single/web-design/img-2.jpg',
            Simg3:'images/service-single/web-design/img-3.jpg',
            des:"Identification des enjeux d'un projet et des clients, Compréhension et Définitions des besoins utilisateurs via des observations, interviews, analyse des environnements et des utilisateurs, …"
        },
        {
            Id:"2",
            sIcon:"fi flaticon-web-design",
            heading:"UI Design",
            Simg1:'images/service-single/development/img-1.jpg',
            Simg2:'images/service-single/development/img-2.jpg',
            Simg3:'images/service-single/development/img-3.jpg',
            des:"Conception de zoning et wireframes, Réalisation des maquettes et prototypage de l'interface pour des rendus statiques ou dynamiques tout support (ordinateurs, tablettes, téléphones...), ..."
        },
        {
            Id:"3",
            sIcon:"fi flaticon-coding",
            heading:"Développement Front",
            Simg1:'images/service-single/creative/img-1.jpg',
            Simg2:'images/service-single/creative/img-2.jpg',
            Simg3:'images/service-single/creative/img-3.jpg',
            des:"Réalisation et optimisation de sites internets côté front (HTML/CSS/JS), Maitrise de frameworks modernes comme React, NextJS ou React Native, Expert en CMS (Wordpress, Woocommerce), Utilisation d'outils de versioning (Git, Gitlab, Bitbucket"        
        },
        {
            Id:"4",
            sIcon:"fi flaticon-smartphone",
            heading:"User Testing",
            Simg1:'images/service-single/responsive/img-1.jpg',
            Simg2:'images/service-single/responsive/img-2.jpg',
            Simg3:'images/service-single/responsive/img-3.jpg',
            des:"Réalisation de tests utilisateurs itérative , In situ ou à distance, Compréhension et correction via les feedbacks, AB Testing, Maitrises d'outils d'analyse d'expérience utilisateur (HotJar)..."        
        },
        {
            Id:"5",
            sIcon:"fi flaticon-iteration",
            heading:"Méthodologie",
            Simg1:'images/service-single/branding/img-1.jpg',
            Simg2:'images/service-single/branding/img-2.jpg',
            Simg3:'images/service-single/branding/img-3.jpg',
            des:"Savoir adopter la bonne méthodologie en fonction du projet. Maitrise de l'Agile, du Design Thinking, Atomic Design ou encore projets en sprint, ... et des outils de préparation et suivi de projet (Scrum Poker, Trello, Jira)."      
        },
        {
            Id:"6",
            sIcon:"fi flaticon-molecule",
            heading:"360° Design",
            Simg1:'images/service-single/support/img-1.jpg',
            Simg2:'images/service-single/support/img-2.jpg',
            Simg3:'images/service-single/support/img-3.jpg',
            des:"Conception et Création de visuels et médias de tout type : affiches print, motion design (vidéos, animations...), Réalisation et amélioration du branding client (Logos, Iconographie, Chartes graphiques,...)."    
        }
    ]

    return (
        <div id="service" className="service-area section-padding">
            <div className="borderd"></div>
            <div className="container">
                <div className="col-l2">
                    <div className="section-title section-title2 section-title-black text-center">
                        <span>Mes passions au service de...</span>
                        <h2>Mes compétences</h2>
                    </div>
                </div>
                <div className="row">
                    {service.map((serv, srv) => (
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12" key={srv}>
                            <div className="service-section">
                                <div className="services-wrapper">
                                    <div className="services-icon-wrapper">
                                        {/* 
                                        <div className="service-dot">
                                            <div className="dots"></div>
                                            <div className="dots2"></div>
                                        </div>
                                        */}
                                        <i className={serv.sIcon}></i>
                                    </div>
                                    <div className="service-content">
                                        <h2>{serv.heading}</h2>
                                        <p>{serv.des}</p>
                                        {/*
                                        <Button
                                            className="btn"
                                            onClick={()=> handleClickOpen(serv)}>
                                            En savoir plus
                                        </Button>
                                        */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        ))}
                </div>
            </div>
            <div className="white_svg">
                <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
                    <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
                </svg>
            </div>
            {/*
            <ServiceSingle open={open} onClose={handleClose} title={state.heading} doc={state.doc} image1={state.Simg1} image2={state.Simg2} image3={state.Simg3}/>
            */}
        </div>
    );
}
export default Service;