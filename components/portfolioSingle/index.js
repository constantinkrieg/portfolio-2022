
import React, { Fragment } from 'react';


import { Dialog, Grid, } from '@material-ui/core'
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Image from 'next/image';


const PortfolioSingle = ({ maxWidth, open, onClose, title, doc, cover, image1, image2, image3, type, videosId, text1, text2, text3, quote, job, date }) => {

    const styles = (theme) => ({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

    const [imgModal, setImgModal] = React.useState(false);
    const [focusImg, setFocusImg] = React.useState();

    const DialogTitle = withStyles(styles)((props) => {
        const { children, classes, onClose, ...other } = props;
        return (
            <MuiDialogTitle disableTypography className={classes.root} {...other}>
                <Typography variant="h6">{children}</Typography>
                {onClose ? (
                    <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                        <i className="fa fa-close"></i>
                    </IconButton>
                ) : null}
            </MuiDialogTitle>
        );
    });


    return (
        <Fragment>
            
            <Dialog
                open={open}
                onClose={onClose}
                className="modalWrapper quickview-dialog"
                maxWidth={maxWidth}
            >
                <DialogTitle id="customized-dialog-title" onClose={onClose}>

                </DialogTitle>
                
            
            {imgModal == true &&
                <div className="modal-img-view">
                    <div className="modal-img-close-overlay" onClick={() => setImgModal(false)}></div>
                    <div className="modal-img-close" onClick={() => setImgModal(false)}>
                        <i className="fa fa-close"></i>
                    </div> 
                    <div className="modal-img-container">
                        <Image className="modal-img-element" src={focusImg} alt={title} width="1095" height="810"/>
                    </div>
                </div>
            }
                <Grid className="modalBody modal-body">
                    <div className="tp-project-details-area">

                        <div className="row">
                            <div className="col-lg-12">
                                <div className="tp-minimal-wrap">
                                    <div className="tp-minimal-img">
                                        <div className="img-clickable">
                                        <Image 
                                            src={image1} 
                                            alt="image-1" 
                                            width="980" 
                                            height="550"
                                            onClick={() => {
                                                setImgModal(true);
                                                setFocusImg(image1);
                                            }} 
                                        />
                                        </div>
                                    </div>
                                    <h2>{title}</h2>
                                </div>
                                
                                <div className="tp-project-details-list">
                                    <div className="row">
                                        <div className="co-l-lg-4 col-md-4 col-sm-6 col-12">
                                            <div className="tp-project-details-text">
                                                <span>Type</span>
                                                <h2>{type}</h2>
                                            </div>
                                        </div>
                                        <div className="co-l-lg-4 col-md-4 col-sm-6 col-12">
                                            <div className="tp-project-details-text-3">
                                                <span>Rôle</span>
                                                <h2>{job}</h2>
                                            </div>
                                        </div>
                                        <div className="co-l-lg-4 col-md-4 col-sm-6 col-12">
                                            <div className="tp-project-details-text">
                                                <span>Date</span>
                                                <h2>{date}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tp-p-details-section">
                                    <p>{text1}</p>
                                    
                                <div className="row">
                                        <div className="col-md-6 col-sm-6 col-12">
                                            <div className="tp-p-details-img">
                                                <div className="img-clickable">
                                                    <Image 
                                                    src={image2} 
                                                    alt="image-2" 
                                                    width="430" 
                                                    height="315"
                                                    onClick={() => {
                                                        setImgModal(true);
                                                        setFocusImg(image2);
                                                    }} 
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-12">
                                            <div className="tp-p-details-img">
                                            <div className="img-clickable">
                                                    <Image 
                                                    src={image3} 
                                                    alt="image-3" 
                                                    width="430" 
                                                    height="315"
                                                    onClick={() => {
                                                        setImgModal(true);
                                                        setFocusImg(image3);
                                                    }} 
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>{text2}</p>
                                    <p>{text3}</p>
                                </div>
                                <div className="tp-p-details-quote">
                                    <i className="fa fa-quote-left icon-quote"></i>
                                    <p>
                                        <b>Mon ressenti :</b>
                                        <br/>
                                        {quote}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Grid>
            </Dialog>
        </Fragment>
    );
}
export default PortfolioSingle

